﻿namespace Store.Grpc.Consumer.Interfaces
{
    public interface IServerAddressService
    {
        string GetServerIp();
    }
}