﻿using System;
using System.Linq;
using GR.Core.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Store.Abstractions.Interfaces;
using Store.Grpc.Consumer.Interfaces;
using Store.Grpc.Consumer.Services;
using Store.Grpc.Consumer.Services.Grpc;
using Store.Shared;
using Store.Shared.Enums;
using Store.Shared.Models;

namespace Store.Grpc.Consumer
{
    public class Startup : IServerAddressService
    {
        private IApplicationBuilder _builder;

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGrpc();
            services.AddSingleton<IConsumerService, GrpcConsumerService>();
            services.AddSingleton<IHttpConnectionFeature, HttpConnectionFeature>();
            services.AddSingleton<IServerAddressService>(this);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime applicationLifetime)
        {
            _builder = app;
            applicationLifetime.ApplicationStarted.Register(OnApplicationStarted);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<NotifierRpcService>();

                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });
        }

        private void OnApplicationStarted()
        {
            var subscriberService = _builder.ApplicationServices.GetService<IConsumerService>();
            subscriberService.OnMessageReceived += (sender, eventArgs) =>
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Received: " + eventArgs.Payload.SerializeAsJson());
                Console.ForegroundColor = ConsoleColor.White;
            };

            subscriberService.Connect(Settings.IP, Settings.PORT, 3000);

            if (!subscriberService.IsConnected)
            {
                throw new Exception("Can't connect to broker");
            }

            subscriberService.Subscribe(new SubscribePayload
            {
                CategoryPattern = "cpu.intel.i*",
                PacketType = PacketType.Subscribe,
                QueueId = "Q1"
            });
        }

        public string GetServerIp()
        {
            var serverAddressesFeature = _builder.ServerFeatures.Get<IServerAddressesFeature>();
            return serverAddressesFeature.Addresses.First();
        }
    }
}
