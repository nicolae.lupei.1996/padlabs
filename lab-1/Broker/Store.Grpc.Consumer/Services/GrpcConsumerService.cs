﻿using System;
using System.Net.Http;
using System.Net.Security;
using Grpc.Net.Client;
using Mapster;
using Store.Abstractions.Interfaces;
using Store.Grpc.Broker;
using Store.Grpc.Consumer.Interfaces;
using Store.Shared.EventArgs;
using SubscribePayload = Store.Shared.Models.SubscribePayload;

namespace Store.Grpc.Consumer.Services
{
    public class GrpcConsumerService : IConsumerService
    {
        private ProductSubscriber.ProductSubscriberClient _client;
        private GrpcChannel _channel;

        public EventHandler<MessageReceivedEventArgs> OnMessageReceived { get; set; }
        private readonly IServerAddressService _addressService;

        public GrpcConsumerService(IServerAddressService addressService)
        {
            _addressService = addressService;
        }

        public bool IsConnected { get; private set; }

        public void Connect(string ip, int port, int timeOut)
        {
            _channel = GrpcChannel.ForAddress($"https://{ip}:{port}", new GrpcChannelOptions
            {
                HttpHandler = new SocketsHttpHandler
                {
                    SslOptions = new SslClientAuthenticationOptions
                    {
                        RemoteCertificateValidationCallback = (sender, certificate, chain, errors) => true
                    }
                }
            });
            _client = new ProductSubscriber.ProductSubscriberClient(_channel);
            IsConnected = true;
        }

        public void Subscribe(SubscribePayload payload)
        {
            var request = payload.Adapt<Broker.SubscribePayload>();
            request.Address = _addressService.GetServerIp();

            try
            {
                var grpcServerResponse = _client.Subscribe(request);
                if (grpcServerResponse.IsSuccess)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Subscribe was with success");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Fail to subscribe: " + grpcServerResponse.ErrorMessage);
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error was occurred: " + e.Message);
            }
        }

        public void StartReceive()
        {
            throw new NotImplementedException();
        }
    }
}
