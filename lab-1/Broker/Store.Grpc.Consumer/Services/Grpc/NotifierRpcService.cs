﻿using System.Threading.Tasks;
using Grpc.Core;
using Mapster;
using Store.Abstractions.Interfaces;
using Store.Grpc.Broker;
using Store.Shared.EventArgs;

namespace Store.Grpc.Consumer.Services.Grpc
{
    public class NotifierRpcService : ProductNotifier.ProductNotifierBase
    {
        private readonly IConsumerService _consumerService;

        public NotifierRpcService(IConsumerService consumerService)
        {
            _consumerService = consumerService;
        }

        public override Task<NotificationResult> Notify(ProductNotification request, ServerCallContext context)
        {
            var data = request.Adapt<Store.Shared.Models.ProductPayload>();
            _consumerService.OnMessageReceived?.Invoke(null, new MessageReceivedEventArgs
            {
                ConnectionInfo = null,
                Payload = data
            });
            return Task.FromResult(new NotificationResult
            {
                IsSuccess = true,
                ErrorMessage = string.Empty
            });
        }
    }
}
