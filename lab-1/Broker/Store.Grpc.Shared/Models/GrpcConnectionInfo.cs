﻿using Grpc.Net.Client;
using Store.Shared.Models;
using System.Net.Http;

namespace Store.Grpc.Shared.Models
{
    public class GrpcConnectionInfo : BaseConnectionInfo
    {
        public GrpcChannel GrpcChannel => GrpcChannel.ForAddress(Address, new GrpcChannelOptions
        {
            HttpHandler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            }
        });
    }
}