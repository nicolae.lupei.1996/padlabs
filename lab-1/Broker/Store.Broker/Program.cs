﻿using System;
using Store.Abstractions.BaseServices;
using Store.Shared;
using Store.TcpBroker.Services;

namespace Store.TcpBroker
{
    class Program
    {
        static void Main(string[] args)
        {
            var subscribeStorage = new InMemorySubscribersStorage();
            var redisStorage = new RedisPayloadStorage();
            var queueManager = new QueueManager(redisStorage);

            var brokerService = new BrokerService(subscribeStorage, queueManager);
            brokerService.OnMessageReceived += (sender, messageReceivedEventArgs) =>
             {
                 //Here can handle received messages
             };

            Console.WriteLine("Tcp Broker started ...");
            brokerService.Start(Settings.IP, Settings.PORT);

            Console.ReadLine();
        }
    }
}