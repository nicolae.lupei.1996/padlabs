﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using GR.Core.Extensions;
using GR.Core.Helpers;
using Newtonsoft.Json.Linq;
using Store.Abstractions;
using Store.Abstractions.Interfaces;
using Store.Shared;
using Store.Shared.Enums;
using Store.Shared.EventArgs;
using Store.Shared.Models;

namespace Store.TcpBroker.Services
{
    public class BrokerService : BaseBroker<TcpConnectionInfo>
    {
        /// <summary>
        /// Socket
        /// </summary>
        private Socket _socket;

        private const int CONNECTIONS_LIMIT = 8;

        public BrokerService(ISubscribersStorage subscribersStorage, IQueueManager queueManager) : base(subscribersStorage, queueManager)
        {
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public override void Start(string ip, int port)
        {
            _socket.Bind(new IPEndPoint(IPAddress.Parse(ip), port));
            _socket.Listen(CONNECTIONS_LIMIT);
            Accept();
        }

        /// <summary>
        /// Push product
        /// </summary>
        /// <param name="productPayload"></param>
        /// <param name="subscriber"></param>
        /// <returns></returns>
        public override ResultModel PublishProductToSubscriber(ProductPayload productPayload, TcpConnectionInfo subscriber)
        {
            var response = new ResultModel();
            var payloadString = productPayload.SerializeAsJson();
            var payloadBytes = Encoding.UTF8.GetBytes(payloadString);
            try
            {
                subscriber.Socket.Send(payloadBytes);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                response.AddError(e.Message);
            }

            return response;
        }

        private void Accept()
        {
            _socket.BeginAccept(BeginAcceptCallback, null);
        }

        #region Callback

        private void BeginAcceptCallback(IAsyncResult ar)
        {
            var connection = new TcpConnectionInfo();

            try
            {
                connection.Socket = _socket.EndAccept(ar);
                connection.Address = connection.Socket.RemoteEndPoint.ToString();
                connection.Socket.BeginReceive(connection.Data, 0, Settings.BUFF_SIZE, SocketFlags.None,
                    ReceiveCallback, connection);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Broker error, {e.Message}");
            }
            finally
            {
                Accept();
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            var connection = (TcpConnectionInfo)ar.AsyncState;
            if (connection == null) throw new NullReferenceException("Connection can't be null");
            try
            {
                var senderSocket = connection.Socket;
                var buffSize = senderSocket.EndReceive(ar, out var response);
                if (response == SocketError.Success)
                {
                    var payload = new byte[Settings.BUFF_SIZE];
                    Array.Copy(connection.Data, payload, buffSize);
                    var payloadString = Encoding.UTF8.GetString(payload);
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;
                    Console.WriteLine("Received: " + payloadString);
                    Console.ForegroundColor = ConsoleColor.White;

                    var jToken = JToken.Parse(payloadString);
                    var packetType = jToken.SelectToken("packetType").Value<int>();
                    BasePayload data = null;
                    switch ((PacketType)packetType)
                    {
                        case PacketType.Subscribe:
                            data = payloadString.Deserialize<SubscribePayload>();
                            break;
                        case PacketType.Push:
                            data = payloadString.Deserialize<ProductPayload>();
                            break;
                    }

                    if (data == null) return;

                    MessageReceived(new MessageReceivedEventArgs
                    {
                        ConnectionInfo = connection,
                        Payload = data
                    });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Broker error, {e.Message}");
            }
            finally
            {
                try
                {
                    connection.Socket.BeginReceive(connection.Data, 0, Settings.BUFF_SIZE, SocketFlags.None,
                        ReceiveCallback, connection);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Broker error, {e.Message}");
                    var address = connection.Address;
                    connection.Socket.Close();
                }
            }
        }

        #endregion
    }
}
