﻿using GR.Core.Extensions;
using Store.Abstractions.Interfaces;
using Store.TcpBroker.Helpers;

namespace Store.TcpBroker.Services
{
    public class RedisPayloadStorage : IPayloadStorage
    {
        public void Save<T>(T entry) where T : class
        {
            var data = entry.SerializeAsJson();
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            cache.StringSet("broker_queue", data);
        }

        public T Get<T>() where T : class
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            var data = cache.StringGet("broker_queue");
            return data.ToString().Deserialize<T>();
        }
    }
}