﻿using GR.Core.Extensions;

namespace Store.TcpBroker.Helpers
{
    public static class RedisMemoryStorage
    {
        public static void Save<T>(T entry)
        {
            var data = entry.SerializeAsJson();
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            cache.StringSet("broker_queue", data);
        }

        public static T Get<T>()
            where T : class
        {
            var cache = RedisConnectorHelper.Connection.GetDatabase();
            var data = cache.StringGet("broker_queue");
            return data.ToString().Deserialize<T>();
        }
    }
}