﻿using System;
using GR.Core.Extensions;
using Store.Abstractions.Interfaces;
using Store.Shared;
using Store.Shared.Enums;
using Store.Shared.Models;
using Store.TcpConsumer.Services;

namespace Store.TcpConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Receiver start...");

            IConsumerService service = new ConsumerService();

            service.OnMessageReceived += (sender, eventArgs) =>
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Received: " + eventArgs.Payload.SerializeAsJson());
                Console.ForegroundColor = ConsoleColor.White;
            };

            service.Connect(Settings.IP, Settings.PORT, 3000);

            if (!service.IsConnected)
            {
                throw new Exception("Can't connect to broker");
            }

            service.Subscribe(new SubscribePayload
            {
                CategoryPattern = "cpu.intel.i*",
                PacketType = PacketType.Subscribe,
                QueueId = "Q1"
            });

            service.StartReceive();

            Console.ReadKey();
        }
    }
}
