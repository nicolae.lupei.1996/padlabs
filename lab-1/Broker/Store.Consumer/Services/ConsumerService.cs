﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using GR.Core.Extensions;
using Store.Abstractions.Interfaces;
using Store.Shared;
using Store.Shared.EventArgs;
using Store.Shared.Models;

namespace Store.TcpConsumer.Services
{
    public class ConsumerService : IConsumerService
    {
        /// <summary>
        /// Socket
        /// </summary>
        private Socket _socket;

        /// <summary>
        /// Check if socket is connected to broker
        /// </summary>
        public bool IsConnected { get; private set; }

        /// <summary>
        /// On message receive
        /// </summary>
        public EventHandler<MessageReceivedEventArgs> OnMessageReceived { get; set; }

        /// <summary>
        /// Patterns
        /// </summary>
        public List<string> SubscribePatterns = new List<string>();

        public ConsumerService()
        {
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Connect(string ip, int port, int timeOut)
        {
            _socket.BeginConnect(new IPEndPoint(IPAddress.Parse(ip), port), ConnectCallback, null);
            Thread.Sleep(timeOut);
        }

        /// <summary>
        /// Push product
        /// </summary>
        /// <param name="payload"></param>
        public void Subscribe(SubscribePayload payload)
        {
            var bytes = Encoding.UTF8.GetBytes(payload.SerializeAsJson());
            try
            {
                _socket.Send(bytes);
                SubscribePatterns.Add(payload.CategoryPattern);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Fail to send, {e.Message}");
            }
        }

        public void StartReceive()
        {
            var connection = new TcpConnectionInfo
            {
                Socket = _socket
            };

            _socket.BeginReceive(connection.Data, 0, Settings.BUFF_SIZE, SocketFlags.None, ReceiveCallback, connection);
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            var connection = (TcpConnectionInfo)ar.AsyncState;
            if (connection == null) throw new NullReferenceException("Connection can't be null");
            try
            {
                var senderSocket = connection.Socket;
                var buffSize = senderSocket.EndReceive(ar, out var response);
                if (response == SocketError.Success)
                {
                    var payload = new byte[Settings.BUFF_SIZE];
                    Array.Copy(connection.Data, payload, buffSize);
                    var payloadString = Encoding.UTF8.GetString(payload);
                    var data = payloadString.Deserialize<ProductPayload>();
                    if (data == null) return;

                    OnMessageReceived?.Invoke(null, new MessageReceivedEventArgs
                    {
                        ConnectionInfo = connection,
                        Payload = data
                    });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Broker error, {e.Message}");
            }
            finally
            {
                try
                {
                    connection.Socket.BeginReceive(connection.Data, 0, Settings.BUFF_SIZE, SocketFlags.None,
                        ReceiveCallback, connection);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Broker error, {e.Message}");
                    connection.Socket.Close();
                }
            }
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            if (_socket.Connected)
            {
                Console.WriteLine("Socket is connected to Broker.");
            }
            else
            {
                Console.WriteLine("Error. Unavailable broker, try late");
            }

            IsConnected = _socket.Connected;
        }
    }
}
