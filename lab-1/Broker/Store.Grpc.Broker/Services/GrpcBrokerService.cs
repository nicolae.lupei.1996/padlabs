﻿using System;
using GR.Core.Helpers;
using Mapster;
using Store.Abstractions;
using Store.Abstractions.Interfaces;
using Store.Grpc.Shared.Models;

namespace Store.Grpc.Broker.Services
{
    public class GrpcBrokerService : BaseBroker<GrpcConnectionInfo>
    {
        public GrpcBrokerService(ISubscribersStorage subscribersStorage, IQueueManager queueManager) : base(subscribersStorage, queueManager)
        {
        }

        public override void Start(string ip, int port)
        {
            //do nothing
        }

        public override ResultModel PublishProductToSubscriber(Store.Shared.Models.ProductPayload productPayload, GrpcConnectionInfo subscriber)
        {
            var result = new ResultModel();
            var request = productPayload.Adapt<ProductNotification>();
            try
            {
                var client = new ProductNotifier.ProductNotifierClient(subscriber.GrpcChannel);
                var notifyResult = client.Notify(request);
                if (notifyResult.IsSuccess)
                {
                    result.IsSuccess = true;
                    return result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result.AddError(e.Message);
            }

            return result;
        }
    }
}