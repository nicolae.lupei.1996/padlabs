using System.Threading.Tasks;
using Grpc.Core;
using Mapster;
using Microsoft.Extensions.Logging;
using Store.Abstractions.Interfaces;
using Store.Grpc.Shared.Models;
using Store.Shared.EventArgs;

namespace Store.Grpc.Broker.Services.Rpc
{
    public class PublishRpcService : ProductPublisher.ProductPublisherBase
    {
        #region Injectable

        private readonly ILogger<PublishRpcService> _logger;
        private readonly IBrokerService<GrpcConnectionInfo> _brokerService;

        #endregion

        public PublishRpcService(ILogger<PublishRpcService> logger, IBrokerService<GrpcConnectionInfo> brokerService)
        {
            _logger = logger;
            _brokerService = brokerService;
        }

        public override Task<PublishResult> PublishProduct(ProductPayload request, ServerCallContext context)
        {
            var product = request.Adapt<Store.Shared.Models.ProductPayload>();
            _brokerService.MessageReceived(new MessageReceivedEventArgs
            {
                Payload = product
            });

            _logger.LogInformation("New Product was published");

            return Task.FromResult(new PublishResult
            {
                IsSuccess = true,
                ErrorMessage = ""
            });
        }
    }
}
