﻿using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Mapster;
using Store.Abstractions.Interfaces;
using Store.Grpc.Shared.Models;

namespace Store.Grpc.Broker.Services.Rpc
{
    public class SubscribersRpcService : BrokerSubscribers.BrokerSubscribersBase
    {
        private readonly ISubscribersStorage _subscribersStorage;

        public SubscribersRpcService(ISubscribersStorage subscribersStorage)
        {
            _subscribersStorage = subscribersStorage;
        }

        public override Task<SubscribersResult> GetSubscribers(GetSubscribersRequest request, ServerCallContext context)
        {
            var result = new SubscribersResult();
            var allSubscribers = _subscribersStorage.GetSubscribers<GrpcConnectionInfo>()
                .Select(x => x.Adapt<SubscriberItem>());
            result.Data.AddRange(allSubscribers);
            return Task.FromResult(result);
        }
    }
}