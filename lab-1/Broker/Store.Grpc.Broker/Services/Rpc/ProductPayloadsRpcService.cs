﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Mapster;
using Store.Abstractions.Interfaces;
using Store.Grpc.Shared.Models;

namespace Store.Grpc.Broker.Services.Rpc
{
    public class ProductPayloadsRpcService : ProductPayloadService.ProductPayloadServiceBase
    {
        #region Injectable

        private readonly IQueueManager _queueManager;
        private readonly IBrokerService<GrpcConnectionInfo> _brokerService;

        #endregion

        public ProductPayloadsRpcService(IQueueManager queueManager, IBrokerService<GrpcConnectionInfo> brokerService)
        {
            _queueManager = queueManager;
            _brokerService = brokerService;
        }

        public override Task<PayloadResult> GetPayloads(ProductRequest request, ServerCallContext context)
        {
            var data = _queueManager.Queues.SelectMany(x => x.Value).ToList();
            var result = new PayloadResult();
            result.Data.AddRange(data.Adapt<IEnumerable<Payload>>());

            return Task.FromResult(result);
        }

        public override Task<PayloadSeconds> GetSeconds(ProductRequest request, ServerCallContext context)
        {
            return Task.FromResult(new PayloadSeconds
            {
                Seconds = _brokerService.GetWorkerIntervalSeconds()
            });
        }

        public override Task<ProductRequest> UpdateInterval(PayloadSeconds request, ServerCallContext context)
        {
            _brokerService.ChangeTime(request.Seconds);
            return Task.FromResult(new ProductRequest());
        }
    }
}