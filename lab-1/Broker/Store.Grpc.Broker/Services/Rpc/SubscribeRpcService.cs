﻿using System.Threading.Tasks;
using Grpc.Core;
using Mapster;
using Microsoft.Extensions.Logging;
using Store.Abstractions.Interfaces;
using Store.Grpc.Shared.Models;

namespace Store.Grpc.Broker.Services.Rpc
{
    public class SubscribeRpcService : ProductSubscriber.ProductSubscriberBase
    {
        #region Injectable

        private readonly ILogger<SubscribeRpcService> _logger;
        private readonly IBrokerService<GrpcConnectionInfo> _brokerService;

        #endregion

        public SubscribeRpcService(ILogger<SubscribeRpcService> logger, IBrokerService<GrpcConnectionInfo> brokerService)
        {
            _logger = logger;
            _brokerService = brokerService;
        }

        public override Task<SubscribeResult> Subscribe(SubscribePayload request, ServerCallContext context)
        {
            var subscribePayload = request.Adapt<Store.Shared.Models.SubscribePayload>();

            _brokerService.Subscribe(subscribePayload, new GrpcConnectionInfo
            {
                Address = request.Address,
                QueueId = request.QueueId
            });

            return Task.FromResult(new SubscribeResult
            {
                IsSuccess = true,
                ErrorMessage = string.Empty
            });
        }
    }
}
