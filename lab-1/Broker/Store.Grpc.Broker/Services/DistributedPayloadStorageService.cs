﻿using System;
using GR.Core.Extensions;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.DependencyInjection;
using Store.Abstractions.Interfaces;

namespace Store.Grpc.Broker.Services
{
    public class DistributedPayloadStorageService : IPayloadStorage
    {
        private readonly IDistributedCache _distributedCache;

        public DistributedPayloadStorageService(IServiceProvider serviceProvider)
        {
            _distributedCache = serviceProvider.GetRequiredService<IDistributedCache>();
        }

        public void Save<T>(T entry) where T : class
        {
            var data = entry.SerializeAsJson();
            _distributedCache.SetStringAsync("broker_queue", data);
        }

        public T Get<T>() where T : class
        {
            var data = _distributedCache.GetString("broker_queue");
            return data.Deserialize<T>();
        }
    }
}
