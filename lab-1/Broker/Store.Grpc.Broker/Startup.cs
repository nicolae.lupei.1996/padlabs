﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Store.Abstractions.BaseServices;
using Store.Abstractions.Interfaces;
using Store.Grpc.Broker.Services;
using Store.Grpc.Broker.Services.Rpc;
using Store.Grpc.Shared.Models;

namespace Store.Grpc.Broker
{
    public class Startup
    {
        protected readonly IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedRedisCache(option =>
            {
                option.Configuration = Configuration["RedisCache:ConnectionString"];
                option.InstanceName = "Broker";
            });

            services.AddGrpc();
            services.AddSingleton<IBrokerService<GrpcConnectionInfo>, GrpcBrokerService>();
            services.AddSingleton<IQueueManager, QueueManager>();
            services.AddSingleton<IPayloadStorage, DistributedPayloadStorageService>();
            services.AddSingleton<ISubscribersStorage, InMemorySubscribersStorage>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<PublishRpcService>();
                endpoints.MapGrpcService<SubscribeRpcService>();
                endpoints.MapGrpcService<SubscribersRpcService>();
                endpoints.MapGrpcService<ProductPayloadsRpcService>();

                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                });
            });
        }
    }
}
