﻿using System.Collections.Generic;

namespace Store.Grpc.Broker.Web.Models
{
    public class DataViewModel
    {
        public IEnumerable<SubscriberItem> SubscriberItems { get; set; }
        public IEnumerable<Payload> ProductPayloads { get; set; }
        public double Interval { get; set; }
    }
}
