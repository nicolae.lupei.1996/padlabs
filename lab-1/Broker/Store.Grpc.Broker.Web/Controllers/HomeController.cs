﻿using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Security;
using System.Threading.Tasks;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Store.Grpc.Broker.Web.Models;
using Store.Shared;

namespace Store.Grpc.Broker.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly BrokerSubscribers.BrokerSubscribersClient _subscribersClient;
        private readonly ProductPayloadService.ProductPayloadServiceClient _payloadsRpcServiceClient;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            var channel = GrpcChannel.ForAddress($"https://{Settings.IP}:{Settings.PORT}", new GrpcChannelOptions
            {
                HttpHandler = new SocketsHttpHandler
                {
                    SslOptions = new SslClientAuthenticationOptions
                    {
                        RemoteCertificateValidationCallback = (sender, certificate, chain, errors) => true
                    }
                }
            });
            _subscribersClient = new BrokerSubscribers.BrokerSubscribersClient(channel);
            _payloadsRpcServiceClient = new ProductPayloadService.ProductPayloadServiceClient(channel);
        }

        public async Task<IActionResult> Index()
        {
            var subscribers = await _subscribersClient.GetSubscribersAsync(new GetSubscribersRequest());
            var payloads = await _payloadsRpcServiceClient.GetPayloadsAsync(new ProductRequest());
            var interval = await _payloadsRpcServiceClient.GetSecondsAsync(new ProductRequest());
            return View(new DataViewModel
            {
                SubscriberItems = subscribers.Data.ToList(),
                ProductPayloads = payloads.Data.ToList(),
                Interval = interval.Seconds
            });
        }

        [HttpPost]
        public async Task<IActionResult> ChangeTime(double seconds)
        {
            await _payloadsRpcServiceClient.UpdateIntervalAsync(new PayloadSeconds
            {
                Seconds = seconds
            });

            return Ok();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
