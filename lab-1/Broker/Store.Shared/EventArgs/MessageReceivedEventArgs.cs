﻿using Store.Shared.Models;

namespace Store.Shared.EventArgs
{
    public class MessageReceivedEventArgs : System.EventArgs
    {
        public virtual BasePayload Payload { get; set; }
        public virtual BaseConnectionInfo ConnectionInfo { get; set; }
    }
}