﻿namespace Store.Shared.Models
{
    public class SubscribePayload : BasePayload
    {
        public virtual string CategoryPattern { get; set; }
    }
}
