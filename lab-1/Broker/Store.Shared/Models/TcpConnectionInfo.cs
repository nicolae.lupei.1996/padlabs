﻿using System.Net.Sockets;

namespace Store.Shared.Models
{
    public class TcpConnectionInfo : BaseConnectionInfo
    {
        public virtual Socket Socket { get; set; }
        public virtual byte[] Data { get; set; } = new byte[Settings.BUFF_SIZE];
    }
}