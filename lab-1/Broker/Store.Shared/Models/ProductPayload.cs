﻿using Store.Shared.Enums;

namespace Store.Shared.Models
{
    public class ProductPayload : BasePayload
    {
        public virtual PersistenceType PersistenceType { get; set; }
        public virtual string Id { get; set; }
        public virtual string Category { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
    }
}