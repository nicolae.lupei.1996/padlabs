﻿using Store.Shared.Enums;

namespace Store.Shared.Models
{
    public class BasePayload
    {
        public virtual string QueueId { get; set; }
        public virtual PacketType PacketType { get; set; }
    }
}
