﻿using System;

namespace Store.Shared.Models
{
    public abstract class BaseConnectionInfo
    {
        protected BaseConnectionInfo()
        {
            ConnectionId = Guid.NewGuid();
        }

        public Guid ConnectionId { get; }
        public string Address { get; set; }
        public string QueueId { get; set; }
    }
}