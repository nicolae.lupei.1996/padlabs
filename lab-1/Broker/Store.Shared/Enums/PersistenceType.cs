﻿namespace Store.Shared.Enums
{
    public enum PersistenceType
    {
        Transient, Persistent
    }
}