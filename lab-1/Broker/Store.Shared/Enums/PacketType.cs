﻿namespace Store.Shared.Enums
{
    public enum PacketType
    {
        Subscribe, Push, Receive
    }
}