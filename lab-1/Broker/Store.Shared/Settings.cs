﻿using System;

namespace Store.Shared
{
    public class Settings
    {
        public const int BUFF_SIZE = 1024;

        public const string IP = "127.0.0.1";

        public const int PORT = 55555;
    }
}