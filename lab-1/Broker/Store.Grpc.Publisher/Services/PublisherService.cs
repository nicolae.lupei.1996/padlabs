﻿using System;
using System.Net.Http;
using System.Net.Security;
using Grpc.Net.Client;
using Mapster;
using Store.Abstractions.Interfaces;
using Store.Grpc.Broker;
using ProductPayload = Store.Shared.Models.ProductPayload;

namespace Store.Grpc.Publisher.Services
{
    public class PublisherService : IPublisherService
    {
        private ProductPublisher.ProductPublisherClient _client;
        private GrpcChannel _channel;

        public bool IsConnected { get; private set; }

        public void Connect(string ip, int port, int timeOut)
        {
            _channel = GrpcChannel.ForAddress($"https://{ip}:{port}", new GrpcChannelOptions
            {
                HttpHandler = new SocketsHttpHandler
                {
                    SslOptions = new SslClientAuthenticationOptions
                    {
                        RemoteCertificateValidationCallback = (sender, certificate, chain, errors) => true
                    }
                }
            });
            _client = new ProductPublisher.ProductPublisherClient(_channel);
            IsConnected = true;
        }

        public void PublishProduct(ProductPayload productPayload)
        {
            var request = productPayload.Adapt<Broker.ProductPayload>();
            try
            {
                var grpcServerResponse = _client.PublishProduct(request);
                if (grpcServerResponse.IsSuccess)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Product {productPayload.Name} with id {productPayload.Id} was published");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Fail to publish product: " + grpcServerResponse.ErrorMessage);
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An error was occurred: " + e.Message);
            }
        }
    }
}