﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Store.Abstractions.Interfaces;
using Store.Shared.Models;

namespace Store.Abstractions.BaseServices
{
    public class QueueManager : IQueueManager
    {
        private readonly IPayloadStorage _storage;
        public ConcurrentDictionary<string, ConcurrentQueue<ProductPayload>> Queues { get; private set; }
        protected bool IsFirstRun = true;

        public QueueManager(IPayloadStorage storage)
        {
            _storage = storage;
            Queues = new ConcurrentDictionary<string, ConcurrentQueue<ProductPayload>>();
        }

        public void PushPayload(ProductPayload payload)
        {
            if (Queues.IsEmpty && IsFirstRun)
            {
                var fromCache = _storage.Get<IEnumerable<KeyValuePair<string, ConcurrentQueue<ProductPayload>>>>();
                if (fromCache != null)
                    Queues = new ConcurrentDictionary<string, ConcurrentQueue<ProductPayload>>(fromCache);
                IsFirstRun = false;
            }
            var queueId = payload.QueueId + "_" + payload.PersistenceType;
            if (Queues.ContainsKey(queueId))
            {
                Queues[queueId].Enqueue(payload);
            }
            else
            {
                Queues.TryAdd(queueId, new ConcurrentQueue<ProductPayload>(new List<ProductPayload> { payload }));
            }

            var toSave = Queues.Where(x => x.Key.EndsWith("_Persistent"));
            _storage.Save(toSave);
        }
    }
}
