﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using GR.Core.Helpers;
using Mapster;
using Store.Abstractions.Interfaces;
using Store.Shared.Models;

namespace Store.Abstractions.BaseServices
{
    public class InMemorySubscribersStorage : ISubscribersStorage
    {
        /// <summary>
        /// Storage
        /// </summary>
        private static readonly ConcurrentDictionary<string, IList<BaseConnectionInfo>> Storage = new ConcurrentDictionary<string, IList<BaseConnectionInfo>>();

        /// <summary>
        /// Subscribe
        /// </summary>
        /// <param name="regexPattern"></param>
        /// <param name="connectionInfo"></param>
        /// <returns></returns>
        public ResultModel Subscribe<T>(string regexPattern, T connectionInfo) where T : BaseConnectionInfo
        {
            var response = new ResultModel();
            if (Storage.ContainsKey(regexPattern))
            {
                Storage[regexPattern].Add(connectionInfo);
            }
            else
            {
                Storage.TryAdd(regexPattern, new List<BaseConnectionInfo>
                {
                    connectionInfo
                });
            }

            return response;
        }

        /// <summary>
        /// Get subscribers
        /// </summary>
        /// <param name="category"></param>
        /// <param name="queueId"></param>
        /// <returns></returns>
        public IEnumerable<T> GetSubscribersForSpecificCategory<T>(string category, string queueId) where T : BaseConnectionInfo
        {
            var data = new List<T>();
            var regexPatterns = Storage.Keys.ToList();
            foreach (var regexPattern in regexPatterns)
            {
                var match = Regex.Match(category, regexPattern);
                if (match.Success)
                {
                    var subscribers = Storage[regexPattern]
                        .Where(x => x.QueueId == queueId && !data.Select(y => y.ConnectionId).Contains(x.ConnectionId)).ToList();
                    if (subscribers.Any()) data.AddRange(subscribers.Cast<T>());
                }
            }

            return data;
        }

        public IEnumerable<T> GetSubscribers<T>() where T : BaseConnectionInfo
        {
            return Storage.SelectMany(x => x.Value).Cast<T>();
        }
    }
}