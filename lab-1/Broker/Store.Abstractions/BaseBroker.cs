﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using GR.Core.Extensions;
using GR.Core.Helpers;
using Store.Abstractions.Interfaces;
using Store.Shared.Enums;
using Store.Shared.EventArgs;
using Store.Shared.Models;

namespace Store.Abstractions
{
    public abstract class BaseBroker<TConnectionInfo> : IBrokerService<TConnectionInfo> where TConnectionInfo : BaseConnectionInfo
    {
        protected readonly ISubscribersStorage SubscribersStorage;
        protected readonly IQueueManager QueueManager;

        public event EventHandler<MessageReceivedEventArgs> OnMessageReceived;

        protected Timer Timer;
        protected double Seconds;

        protected BaseBroker(ISubscribersStorage subscribersStorage, IQueueManager queueManager)
        {
            SubscribersStorage = subscribersStorage;
            QueueManager = queueManager;
            OnMessageReceived += (sender, messageReceivedEventArgs) =>
            {
                switch (messageReceivedEventArgs.Payload.PacketType)
                {
                    case PacketType.Subscribe:
                        var subscribePayload = (SubscribePayload)messageReceivedEventArgs.Payload;
                        messageReceivedEventArgs.ConnectionInfo.QueueId = subscribePayload.QueueId;
                        var connectionInfo = (TConnectionInfo)messageReceivedEventArgs.ConnectionInfo;
                        Subscribe(subscribePayload, connectionInfo);
                        break;
                    case PacketType.Push:
                        var productPayload = (ProductPayload)messageReceivedEventArgs.Payload;
                        if (productPayload == null) return;
                        productPayload.PacketType = PacketType.Receive;
                        QueueManager.PushPayload(productPayload);
                        break;
                }
            };

            var startTimeSpan = TimeSpan.Zero;
            Seconds = 3;
            var periodTimeSpan = TimeSpan.FromSeconds(Seconds);

            Timer = new Timer((e) =>
            {
                DequeuePayload();
            }, null, startTimeSpan, periodTimeSpan);
        }

        public void MessageReceived(MessageReceivedEventArgs e)
        {
            OnMessageReceived?.Invoke(null, e);
        }

        public virtual void ChangeTime(double seconds)
        {
            Seconds = seconds;
            Timer.Change(TimeSpan.Zero, TimeSpan.FromSeconds(seconds));
        }

        public double GetWorkerIntervalSeconds()
        {
            return Seconds;
        }

        public virtual ResultModel Subscribe(SubscribePayload payload, TConnectionInfo connectionInfo)
        {
            var storageResponse = SubscribersStorage.Subscribe(payload.CategoryPattern, connectionInfo);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Subscribed to: {payload.CategoryPattern}");
            Console.ForegroundColor = ConsoleColor.White;
            return storageResponse;
        }

        public virtual IEnumerable<TConnectionInfo> GetSubscribers(string category, string queueId)
        {
            return SubscribersStorage.GetSubscribersForSpecificCategory<TConnectionInfo>(category, queueId);
        }

        public abstract void Start(string ip, int port);

        public abstract ResultModel PublishProductToSubscriber(ProductPayload productPayload, TConnectionInfo subscriber);

        private void DequeuePayload()
        {
            Parallel.ForEach(QueueManager.Queues, queue =>
            {
                queue.Value.TryDequeue(out var productPayload);
                if (productPayload == null) return;
                var subscribers = GetSubscribers(productPayload.Category, productPayload.QueueId);

                Parallel.ForEach(subscribers, subscriber =>
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine($"Send to: {subscriber.ConnectionId}, for {productPayload.Category}, data: {productPayload.SerializeAsJson()}");
                    Console.ForegroundColor = ConsoleColor.White;
                    PublishProductToSubscriber(productPayload, subscriber);
                });
            });
        }
    }
}
