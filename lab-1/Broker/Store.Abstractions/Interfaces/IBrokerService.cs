﻿using System.Collections.Generic;
using GR.Core.Helpers;
using Store.Shared.EventArgs;
using Store.Shared.Models;

namespace Store.Abstractions.Interfaces
{
    public interface IBrokerService<TConnectionInfo> where TConnectionInfo : BaseConnectionInfo
    {
        void Start(string ip, int port);
        ResultModel Subscribe(SubscribePayload payload, TConnectionInfo connectionInfo);
        ResultModel PublishProductToSubscriber(ProductPayload productPayload, TConnectionInfo subscriber);
        IEnumerable<TConnectionInfo> GetSubscribers(string category, string queueId);
        void MessageReceived(MessageReceivedEventArgs e);
        void ChangeTime(double seconds);
        double GetWorkerIntervalSeconds();
    }
}