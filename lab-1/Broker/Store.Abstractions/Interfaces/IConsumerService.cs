﻿using System;
using Store.Shared.EventArgs;
using Store.Shared.Models;

namespace Store.Abstractions.Interfaces
{
    public interface IConsumerService
    {
        EventHandler<MessageReceivedEventArgs> OnMessageReceived { get; set; }
        bool IsConnected { get; }
        void Connect(string ip, int port, int timeOut);
        void Subscribe(SubscribePayload payload);
        void StartReceive();
    }
}
