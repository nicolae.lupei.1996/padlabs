﻿namespace Store.Abstractions.Interfaces
{
    public interface IPayloadStorage
    {
        void Save<T>(T entry) where T : class;
        T Get<T>() where T : class;
    }
}