﻿using System.Collections.Concurrent;
using Store.Shared.Models;

namespace Store.Abstractions.Interfaces
{
    public interface IQueueManager
    {
        ConcurrentDictionary<string, ConcurrentQueue<ProductPayload>> Queues { get; }
        void PushPayload(ProductPayload payload);
    }
}