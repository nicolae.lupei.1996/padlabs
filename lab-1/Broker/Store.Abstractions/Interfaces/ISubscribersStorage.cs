﻿using System.Collections.Generic;
using GR.Core.Helpers;
using Store.Shared.Models;

namespace Store.Abstractions.Interfaces
{
    public interface ISubscribersStorage
    {
        /// <summary>
        /// Subscribe
        /// </summary>
        /// <param name="category"></param>
        /// <param name="connectionInfo"></param>
        /// <returns></returns>
        ResultModel Subscribe<T>(string category, T connectionInfo) where T : BaseConnectionInfo;

        /// <summary>
        /// Get subscribers
        /// </summary>
        /// <param name="category"></param>
        /// <param name="queueId"></param>
        /// <returns></returns>
        IEnumerable<T> GetSubscribersForSpecificCategory<T>(string category, string queueId)
            where T : BaseConnectionInfo;

        /// <summary>
        /// Subscribers
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IEnumerable<T> GetSubscribers<T>()
            where T : BaseConnectionInfo;
    }
}