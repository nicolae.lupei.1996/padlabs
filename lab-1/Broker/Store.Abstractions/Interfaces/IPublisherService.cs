﻿using Store.Shared.Models;

namespace Store.Abstractions.Interfaces
{
    public interface IPublisherService
    {
        bool IsConnected { get; }
        void Connect(string ip, int port, int timeOut);
        void PublishProduct(ProductPayload productPayload);
    }
}
