﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using GR.Core.Extensions;
using Store.Abstractions.Interfaces;
using Store.Shared.Models;

namespace Store.TcpPublisher.Services
{
    public class PublisherService : IPublisherService
    {
        /// <summary>
        /// Socket
        /// </summary>
        private Socket _socket;

        /// <summary>
        /// Check if socket is connected to broker
        /// </summary>
        public bool IsConnected { get; private set; }

        public PublisherService()
        {
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Connect(string ip, int port, int timeOut)
        {
            _socket.BeginConnect(new IPEndPoint(IPAddress.Parse(ip), port), ConnectCallback, null);
            Thread.Sleep(timeOut);
        }

        /// <summary>
        /// Push product
        /// </summary>
        /// <param name="productPayload"></param>
        public void PublishProduct(ProductPayload productPayload)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            var payloadString = productPayload.SerializeAsJson();
            Console.WriteLine("Publish: " + payloadString);
            Console.ForegroundColor = ConsoleColor.White;
            var bytes = Encoding.UTF8.GetBytes(payloadString);
            try
            {
                _socket.Send(bytes);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Fail to send, {e.Message}");
            }
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            if (_socket.Connected)
            {
                Console.WriteLine("Socket is connected to Broker.");
            }
            else
            {
                Console.WriteLine("Error. Unavailable broker, try late");
            }

            IsConnected = _socket.Connected;
        }
    }
}
