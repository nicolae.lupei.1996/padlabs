﻿using System;
using System.Threading;
using Store.Abstractions.Interfaces;
using Store.Shared;
using Store.Shared.Enums;
using Store.Shared.Models;
using Store.TcpPublisher.Services;

namespace Store.TcpPublisher
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tcp Publisher start...");

            IPublisherService service = new PublisherService();
            service.Connect(Settings.IP, Settings.PORT, 3000);

            if (!service.IsConnected)
            {
                throw new Exception("Can't connect to broker");
            }

            while (true)
            {
                service.PublishProduct(new ProductPayload
                {
                    PacketType = PacketType.Push,
                    Category = "cpu.amd",
                    Id = "18090",
                    Name = "Ryzen 9 3900X",
                    Description = "AMD cpu",
                    QueueId = "Q1",
                    PersistenceType = PersistenceType.Transient
                });

                Thread.Sleep(1000);

                service.PublishProduct(new ProductPayload
                {
                    PacketType = PacketType.Push,
                    Category = "cpu.intel.i7",
                    Id = "769698",
                    Name = "I9 9900k",
                    Description = "Intel cpu",
                    QueueId = "Q1",
                    PersistenceType = PersistenceType.Transient
                });

                Thread.Sleep(1000);

                service.PublishProduct(new ProductPayload
                {
                    PacketType = PacketType.Push,
                    Category = "cpu.intel.i3",
                    Id = "769694",
                    Name = "I3 8500H",
                    Description = "Intel cpu",
                    QueueId = "Q2",
                    PersistenceType = PersistenceType.Persistent
                });
            }
        }
    }
}
