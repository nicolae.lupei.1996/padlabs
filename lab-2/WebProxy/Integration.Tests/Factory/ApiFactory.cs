﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Integration.Tests.Factory
{
    public class ApiFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
        private readonly Action<IHost> _hostAction;

        public ApiFactory(Action<IHost> hostAction = null)
        {
            _hostAction = hostAction;
        }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            var startupType = typeof(TStartup);
            var assemblyPath = Assembly.GetAssembly(startupType)?.Location;
            var path = Directory.GetParent(assemblyPath).FullName;
            var serviceName = startupType.Name.Split("TestStartup")[0];
            builder
                .UseStartup<TStartup>()
                .UseContentRoot(path)
                .ConfigureAppConfiguration((context, configBuilder) =>
                {
                    configBuilder.AddJsonFile($"Services/{serviceName}/appsettings.json", false, true);
                    configBuilder.AddEnvironmentVariables();
                })
                .ConfigureTestServices(services =>
                {

                });
        }

        protected override TestServer CreateServer(IWebHostBuilder builder)
        {
            var testServer = new TestServer(builder);
            return testServer;
        }

        protected override IHost CreateHost(IHostBuilder builder)
        {
            var host = builder.Build();
            _hostAction?.Invoke(host);
            host.Start();
            return host;
        }

        protected override IHostBuilder CreateHostBuilder()
        {
            var builder = new HostBuilder();
            return builder;
        }
    }
}
