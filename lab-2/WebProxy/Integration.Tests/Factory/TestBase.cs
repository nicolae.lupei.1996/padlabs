using System.Net.Http;
using System.Text;
using Integration.Tests.Builders;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Integration.Tests.Factory
{
    public class TestBase<TStartup> where TStartup : class
    {
        private ApiFactory<TStartup> _factory;

        protected HttpClient Client { get; set; }
        protected AppSettings Settings { get; set; }
        protected TestServer Server { get; set; }
        protected T Resolve<T>() => _factory.Services.GetService<T>();

        [TestInitialize]
        public virtual void TestInitialize()
        {
            _factory = CreateWebApiFactory();
            Server = _factory.Server;
            Settings = Resolve<IConfiguration>().Get<AppSettings>();

            var opts = new ClientOptionsBuilder(Settings.WebApiConfiguration).Build();

            Client = _factory.CreateClient(opts);
        }

        protected virtual ApiFactory<TStartup> CreateWebApiFactory()
        {
            return new ApiFactory<TStartup>();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _factory?.Dispose();

            Client?.Dispose();
        }

        public static StringContent CreateContent(string payload)
        {
            return new StringContent(payload, Encoding.UTF8, "application/json");
        }
    }
}
