﻿using System;
using FizzWare.NBuilder;
using Microsoft.AspNetCore.Mvc.Testing;

namespace Integration.Tests.Builders
{
    public class ClientOptionsBuilder : Builder
    {
        public ClientOptionsBuilder(WebApiConfiguration configuration)
        {
            var uri = new Uri(configuration.WebApiBaseAddress);

            Options = CreateNew<WebApplicationFactoryClientOptions>()
                .With(o => o.BaseAddress = uri)
                .With(o => o.AllowAutoRedirect = configuration.AllowAutoRedirect);
        }

        public ISingleObjectBuilder<WebApplicationFactoryClientOptions> Options { get; }

        public ClientOptionsBuilder With<TFunc>(Func<WebApplicationFactoryClientOptions, TFunc> func)
        {
            Options.With(func);

            return this;
        }

        public WebApplicationFactoryClientOptions Build()
        {
            return Options.Build();
        }
    }
}
