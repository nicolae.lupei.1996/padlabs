﻿namespace Integration.Tests.Services.Customers
{
    public class CustomersRoutes
    {
        public const string Root = "/";
        public const string Swagger = "/swagger/v1/swagger.json";
    }
}