﻿using CustomersApi;
using Microsoft.Extensions.Configuration;

namespace Integration.Tests.Services.Customers
{
    public class CustomersTestStartup : Startup
    {
        public CustomersTestStartup(IConfiguration configuration) : base(configuration)
        {
        }
    }
}