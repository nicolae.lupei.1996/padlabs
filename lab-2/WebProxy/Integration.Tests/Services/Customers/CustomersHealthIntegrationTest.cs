﻿using System.Threading.Tasks;
using Integration.Tests.Factory;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Integration.Tests.Services.Customers
{
    [TestClass]
    public class CustomersHealthIntegrationTest : TestBase<CustomersTestStartup>
    {
        [TestMethod]
        [DataRow(CustomersRoutes.Root)]
        [DataRow(CustomersRoutes.Swagger)]
        public async Task Get_EndpointsReturnSuccess(string relativeUrl)
        {
            // Act
            var response = await Client.GetAsync(relativeUrl);

            // Assert
            response.EnsureSuccessStatusCode(); // Status Code 200-299
        }
    }
}