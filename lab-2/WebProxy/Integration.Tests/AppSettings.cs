﻿
// ReSharper disable ClassNeverInstantiated.Global
namespace Integration.Tests
{
    public class AppSettings
    {
        public WebApiConfiguration WebApiConfiguration { get; set; }
    }

    /// <summary>
    /// Web Api Configuration
    ///<para><remarks>Optionally, add other WebApplicationFactoryClientOptions</remarks></para>
    /// </summary>
    public class WebApiConfiguration
    {
        public string WebApiBaseAddress { get; set; }
        public bool AllowAutoRedirect { get; set; }
    }


    public class AuthenticationDefaults
    {
        public string DefaultAuthenticateScheme { get; set; }
        public string SubjectId { get; set; }
        public string NameId { get; set; }
        public string Password { get; set; }
    }
}
