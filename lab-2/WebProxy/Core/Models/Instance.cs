﻿using Core.Enums;

namespace Core.Models
{
    public class Instance
    {
        public string InstanceUrl { get; set; }
        public InstanceStatus Status { get; set; } = InstanceStatus.Up;
        public int RequestCount { get; set; }
        public int RequestsPerMinute { get; set; }
        public int MaxRequestsPerMinute { get; set; }
        public double LoadPercentage { get; set; }
        public string InstanceType { get; set; }
    }
}