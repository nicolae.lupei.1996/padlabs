﻿namespace Core.Dto
{
    public class RegisterInstanceDto
    {
        public string InstanceUrl { get; set; }
        public int MaxRequestsPerMinute { get; set; }
        public string InstanceType { get; set; }
    }
}