﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Enums;
using Core.Models;
using Proxy.Abstractions;

namespace Proxy.Services
{
    public class HostsService : IHostsService
    {
        private static readonly ConcurrentStack<Instance> Instances = new ConcurrentStack<Instance>();

        public Task<IEnumerable<Instance>> GetInstancesAsync()
        {
            return Task.FromResult(Instances.AsEnumerable());
        }

        public Task<IEnumerable<Instance>> GetDownInstancesAsync()
        {
            return Task.FromResult(Instances.Where(x => x.Status == InstanceStatus.Down).AsEnumerable());
        }

        public Task RegisterInstanceAsync(Instance instance)
        {
            if (Instances.Any(x => x.InstanceUrl == instance.InstanceUrl))
            {
                return Task.CompletedTask;
            }

            Instances.Push(instance);
            return Task.CompletedTask;
        }
    }
}