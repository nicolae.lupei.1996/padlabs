﻿using System.Threading.Tasks;
using RpcServices;
using Core.Models;
using Grpc.Core;
using Proxy.Abstractions;

namespace Proxy.Rpc
{
    public class RegisterInstanceRpcService : RegisterInstance.RegisterInstanceBase
    {
        #region Injectable

        private readonly IHostsService _hostsService;

        #endregion

        public RegisterInstanceRpcService(IHostsService hostsService)
        {
            _hostsService = hostsService;
        }

        public override async Task<RegisterResult> Register(RegisterConfiguration request, ServerCallContext context)
        {
            await _hostsService.RegisterInstanceAsync(new Instance
            {
                InstanceUrl = request.InstanceUrl,
                MaxRequestsPerMinute = request.MaxRequestsPerMinute,
                InstanceType = request.InstanceType
            });

            return new RegisterResult
            {
                IsSuccess = true
            };
        }
    }
}
