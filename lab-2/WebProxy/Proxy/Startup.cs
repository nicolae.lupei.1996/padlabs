using Coravel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Proxy.Abstractions;
using Proxy.BackgroundServices;
using Proxy.Middlewares;
using Proxy.Rpc;
using Proxy.Services;

namespace Proxy
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddControllers();
            services.AddGrpc();
            services.AddSingleton<IHostsService, HostsService>();
            services.AddScheduler();
            services.AddSingleton<CalculateLoadBackgroundService>();
            services.AddSingleton<RetryInstanceBackgroundService>();

            services.AddDistributedRedisCache(option =>
            {
                option.Configuration = Configuration["RedisCache:ConnectionString"];
                option.InstanceName = "Proxy";
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<RegisterInstanceRpcService>();
                endpoints.MapControllers();
                endpoints.MapGet("/hc", async context =>
                {
                    await context.Response.WriteAsync("Proxy api is up and running!");
                });
                endpoints.MapRazorPages();
            });

            app.UseMiddleware<ReverseProxyMiddleware>();

            var provider = app.ApplicationServices;
            provider.UseScheduler(scheduler =>
            {
                scheduler.Schedule<CalculateLoadBackgroundService>()
                    .EveryMinute();

                scheduler.Schedule<RetryInstanceBackgroundService>()
                    .EveryMinute();
            });
        }
    }
}
