﻿using System;
using System.Net;
using System.Threading.Tasks;
using Coravel.Invocable;
using Core.Enums;
using Proxy.Abstractions;

namespace Proxy.BackgroundServices
{
    public class RetryInstanceBackgroundService : IInvocable
    {
        #region Injectable

        /// <summary>
        /// Inject services
        /// </summary>
        private readonly IHostsService _hostsService;

        #endregion

        public RetryInstanceBackgroundService(IHostsService hostsService)
        {
            _hostsService = hostsService;
        }

        public async Task Invoke()
        {
            var downInstances = await _hostsService.GetDownInstancesAsync();
            foreach (var instance in downInstances)
            {
                if (Ping(instance.InstanceUrl))
                {
                    instance.Status = InstanceStatus.Up;
                }
            }
        }

        private bool Ping(string url)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Timeout = 3000;
                request.AllowAutoRedirect = false; // find out if this site is up and don't follow a redirector
                request.Method = "GET";

                using var response = request.GetResponse();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }
    }
}