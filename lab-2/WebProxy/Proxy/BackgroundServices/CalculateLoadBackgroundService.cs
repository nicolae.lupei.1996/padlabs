﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Coravel.Invocable;
using Proxy.Abstractions;

namespace Proxy.BackgroundServices
{
    public class CalculateLoadBackgroundService : IInvocable
    {
        #region Injectable

        /// <summary>
        /// Inject hosts service
        /// </summary>
        private readonly IHostsService _hostsService;

        #endregion

        public CalculateLoadBackgroundService(IHostsService hostsService)
        {
            _hostsService = hostsService;
        }

        public async Task Invoke()
        {
            var instances = (await _hostsService.GetInstancesAsync()).ToList();
            foreach (var instance in instances)
            {
                var load = instance.RequestsPerMinute * 100D / instance.MaxRequestsPerMinute;
                instance.LoadPercentage = Math.Round(load, 4);
                instance.RequestsPerMinute = 0;
            }
        }
    }
}
