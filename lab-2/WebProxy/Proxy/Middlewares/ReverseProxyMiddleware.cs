﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Core.Enums;
using Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Proxy.Abstractions;

namespace Proxy.Middlewares
{
    public class ReverseProxyMiddleware
    {
        #region Injectable

        private readonly RequestDelegate _nextMiddleware;
        private readonly IHostsService _hostsService;
        private readonly IDistributedCache _distributedCache;

        #endregion

        private static readonly HttpClient HttpClient = new HttpClient
        {
            Timeout = TimeSpan.FromSeconds(3)
        };

        public ReverseProxyMiddleware(RequestDelegate nextMiddleware, IHostsService hostsService, IDistributedCache distributedCache)
        {
            _nextMiddleware = nextMiddleware;
            _hostsService = hostsService;
            _distributedCache = distributedCache;
        }

        private static readonly IEnumerable<string> WhiteListRelativeUrl = new List<string>
        {
            "/api"
        };

        public async Task Invoke(HttpContext context)
        {
            if (!WhiteListRelativeUrl.Any(x => context.Request.Path.Value.StartsWith(x)))
            {
                await _nextMiddleware(context);
                return;
            }

            var completed = false;
            var retryCount = 0;

            do
            {
                var (instance, targetUri) = await GetInstanceUrlAsync(context);

                if (targetUri != null)
                {
                    var targetRequestMessage = CreateTargetMessage(context, targetUri);
                    completed = await ExecuteRequestAsync(context, instance, targetRequestMessage, retryCount++);
                }
                else
                {
                    break;
                }
            } while (completed == false);

            if (completed) return;
            var key = context.Request.Path.Value;
            var cachedData = await _distributedCache.GetStringAsync(key);

            if (cachedData != null)
            {
                context.Response.StatusCode = 200;
                await context.Response.WriteAsync(cachedData);
                return;
            }

            await context.Response.WriteAsync("No instances for execute request");

            await _nextMiddleware(context);
        }

        private async Task<bool> ExecuteRequestAsync(HttpContext context, Instance instance, HttpRequestMessage targetRequestMessage, int retry)
        {
            try
            {
                using var responseMessage = await HttpClient.SendAsync(targetRequestMessage, HttpCompletionOption.ResponseHeadersRead, context.RequestAborted);
                context.Response.StatusCode = (int)responseMessage.StatusCode;

                if (responseMessage.IsSuccessStatusCode)
                {
                    if (retry >= 1 && HttpMethods.IsGet(context.Request.Method))
                    {
                        var stringData = await responseMessage.Content.ReadAsStringAsync();
                        var key = context.Request.Path.Value;
                        await _distributedCache.SetStringAsync(key, stringData);
                    }
                }

                CopyFromTargetResponseHeaders(context, responseMessage);
                await responseMessage.Content.CopyToAsync(context.Response.Body);

                return true;
            }
            catch (Exception e)
            {
                instance.Status = InstanceStatus.Down;
                Console.WriteLine(e);
                return false;
            }
        }

        public async Task<(Instance, Uri)> GetInstanceUrlAsync(HttpContext context)
        {
            var segments = context.Request.Path.Value.Split("/");
            var requestInstanceType = segments[3].ToLower();
            var hosts = (await _hostsService.GetInstancesAsync()).Where(x => x.Status == InstanceStatus.Up && x.InstanceType == requestInstanceType).ToList();
            if (!hosts.Any()) return (null, null);
            var avg = hosts.Average(x => x.RequestsPerMinute);
            var instance = hosts.First(x => x.RequestsPerMinute <= avg);
            instance.RequestCount++;
            instance.RequestsPerMinute++;
            var uri = new Uri(instance.InstanceUrl + context.Request.Path);
            return (instance, uri);
        }

        private HttpRequestMessage CreateTargetMessage(HttpContext context, Uri targetUri)
        {
            var requestMessage = new HttpRequestMessage();
            CopyFromOriginalRequestContentAndHeaders(context, requestMessage);

            requestMessage.RequestUri = targetUri;
            requestMessage.Headers.Host = targetUri.Host;
            requestMessage.Method = GetMethod(context.Request.Method);

            return requestMessage;
        }

        private void CopyFromOriginalRequestContentAndHeaders(HttpContext context, HttpRequestMessage requestMessage)
        {
            var requestMethod = context.Request.Method;

            if (!HttpMethods.IsGet(requestMethod) &&
              !HttpMethods.IsHead(requestMethod) &&
              !HttpMethods.IsDelete(requestMethod) &&
              !HttpMethods.IsTrace(requestMethod))
            {
                var streamContent = new StreamContent(context.Request.Body);
                requestMessage.Content = streamContent;
            }

            foreach (var header in context.Request.Headers)
            {
                requestMessage.Content?.Headers.TryAddWithoutValidation(header.Key, header.Value.ToArray());
            }
        }

        private void CopyFromTargetResponseHeaders(HttpContext context, HttpResponseMessage responseMessage)
        {
            foreach (var header in responseMessage.Headers)
            {
                context.Response.Headers[header.Key] = header.Value.ToArray();
            }

            foreach (var header in responseMessage.Content.Headers)
            {
                context.Response.Headers[header.Key] = header.Value.ToArray();
            }
            context.Response.Headers.Remove("transfer-encoding");
        }
        private static HttpMethod GetMethod(string method)
        {
            if (HttpMethods.IsDelete(method)) return HttpMethod.Delete;
            if (HttpMethods.IsGet(method)) return HttpMethod.Get;
            if (HttpMethods.IsHead(method)) return HttpMethod.Head;
            if (HttpMethods.IsOptions(method)) return HttpMethod.Options;
            if (HttpMethods.IsPost(method)) return HttpMethod.Post;
            if (HttpMethods.IsPut(method)) return HttpMethod.Put;
            if (HttpMethods.IsTrace(method)) return HttpMethod.Trace;
            return new HttpMethod(method);
        }
    }
}
