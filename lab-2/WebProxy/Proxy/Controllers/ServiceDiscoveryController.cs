﻿using System.Threading.Tasks;
using Core.Dto;
using Core.Models;
using Microsoft.AspNetCore.Mvc;
using Proxy.Abstractions;

namespace Proxy.Controllers
{
    [Route("[controller]/[action]")]
    public class ServiceDiscoveryController : ControllerBase
    {
        private readonly IHostsService _hostsService;

        public ServiceDiscoveryController(IHostsService hostsService)
        {
            _hostsService = hostsService;
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegisterInstanceDto request)
        {
            await _hostsService.RegisterInstanceAsync(new Instance
            {
                InstanceUrl = request.InstanceUrl,
                MaxRequestsPerMinute = request.MaxRequestsPerMinute,
                InstanceType = request.InstanceType
            });

            return Ok();
        }
    }
}