﻿using System.Collections.Generic;
using Core.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Proxy.Abstractions;

namespace Proxy.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IHostsService _hostsService;

        public IEnumerable<Instance> Instances { get; set; }

        public IndexModel(ILogger<IndexModel> logger, IHostsService hostsService)
        {
            _logger = logger;
            _hostsService = hostsService;
        }

        public async void OnGet()
        {
            Instances = await _hostsService.GetInstancesAsync();
        }
    }
}
