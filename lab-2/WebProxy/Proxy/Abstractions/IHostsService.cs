﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models;

namespace Proxy.Abstractions
{
    public interface IHostsService
    {
        Task<IEnumerable<Instance>> GetInstancesAsync();
        Task<IEnumerable<Instance>> GetDownInstancesAsync();
        Task RegisterInstanceAsync(Instance instance);
    }
}