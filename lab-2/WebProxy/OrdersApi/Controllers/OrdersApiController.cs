﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OrdersApi.Abstractions;
using OrdersApi.ViewModels;

namespace OrdersApi.Controllers
{
    [Route("/api/v1/orders/[action]")]
    public class OrdersApiController
    {
        #region Injectable

        /// <summary>
        /// Inject order service
        /// </summary>
        private readonly IOrdersService _ordersService;

        #endregion

        public OrdersApiController(IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        [HttpPost]
        public async Task<JsonResult> AddOrder(AddOrderViewModel model)
        {
            return new JsonResult(await _ordersService.AddOrderAsync(model));
        }

        [HttpGet]
        public async Task<JsonResult> GetCustomerOrders(Guid customerId)
        {
            return new JsonResult(await _ordersService.GetCustomerOrdersAsync(customerId));
        }

        [HttpGet]
        public async Task<JsonResult> GetAllOrders()
        {
            return new JsonResult(await _ordersService.GetAllOrdersAsync());
        }
    }
}