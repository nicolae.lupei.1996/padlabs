﻿using AutoMapper;
using OrdersApi.Models;
using OrdersApi.ViewModels;

namespace OrdersApi.Mappers
{
    public class OrdersMapperProfile : Profile
    {
        public OrdersMapperProfile()
        {
            CreateMap<Order, AddOrderViewModel>()
                .IncludeAllDerived()
                .ReverseMap();
        }
    }
}