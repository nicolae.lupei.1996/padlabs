﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GR.Core.Helpers;
using OrdersApi.Models;
using OrdersApi.ViewModels;

namespace OrdersApi.Abstractions
{
    public interface IOrdersService
    {
        /// <summary>
        /// Add order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<IResultModel<Guid>> AddOrderAsync(AddOrderViewModel model);

        /// <summary>
        /// Get orders for specific customer
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        Task<IResultModel<IEnumerable<Order>>> GetCustomerOrdersAsync(Guid customerId);

        /// <summary>
        /// Get all orders
        /// </summary>
        /// <returns></returns>
        Task<IResultModel<IEnumerable<Order>>> GetAllOrdersAsync();
    }
}