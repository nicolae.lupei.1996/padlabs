﻿using System;

namespace OrdersApi.ViewModels
{
    public class AddOrderViewModel
    {
        public Guid CustomerId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public decimal Total { get; set; }
    }
}