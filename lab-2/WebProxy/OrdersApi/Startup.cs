using System;
using System.Linq;
using System.Net.Http;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Core.Dto;
using RpcServices;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using OrdersApi.Abstractions;
using OrdersApi.Data;
using OrdersApi.Services;

namespace OrdersApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<OrdersDbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"));
            });
            services.AddSwaggerGen();
            services.AddAutoMapper(GetType().Assembly);
            services.AddScoped<IOrdersService, OrdersService>();
        }

        private IApplicationBuilder App;

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime applicationLifetime)
        {
            App = app;
            applicationLifetime.ApplicationStarted.Register(OnApplicationStarted);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Customers API");
            });

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Orders api is up and running!");
                });
            });

            UpdateDatabase(app);
        }

        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope();
            using var context = serviceScope.ServiceProvider.GetService<OrdersDbContext>();
            context.Database.Migrate();
        }

        private async void OnApplicationStarted()
        {
            var proxyUrl = Configuration.GetValue<string>("ProxyUrl");
            var url = "http://ordersapi:80";

            var client = new HttpClient();
            var random = new Random();

            var dto = new RegisterInstanceDto
            {
                InstanceUrl = url,
                MaxRequestsPerMinute = random.Next(100, 1000),
                InstanceType = "orders"
            };

            var postUrl = proxyUrl + "/ServiceDiscovery/Register";
            try
            {
                var registerResult = await client.PostAsync(postUrl, new StringContent(JsonConvert.SerializeObject(dto), Encoding.UTF8, "application/json"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public string GetServerIp()
        {
            var serverAddressesFeature = App.ServerFeatures.Get<IServerAddressesFeature>();
            return serverAddressesFeature.Addresses.First();
        }
    }
}
