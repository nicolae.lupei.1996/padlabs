﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GR.Core.Helpers;
using Microsoft.EntityFrameworkCore;
using OrdersApi.Abstractions;
using OrdersApi.Data;
using OrdersApi.Models;
using OrdersApi.ViewModels;

namespace OrdersApi.Services
{
    public class OrdersService : IOrdersService
    {
        #region Injectable

        private readonly IMapper _mapper;
        private readonly OrdersDbContext _context;

        #endregion

        public OrdersService(IMapper mapper, OrdersDbContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        public async Task<IResultModel<Guid>> AddOrderAsync(AddOrderViewModel model)
        {
            var dataModel = _mapper.Map<Order>(model);
            await _context.Orders.AddAsync(dataModel);
            await _context.SaveChangesAsync();
            return new ResultModel<Guid>
            {
                IsSuccess = true,
                Result = dataModel.Id
            };
        }

        public async Task<IResultModel<IEnumerable<Order>>> GetCustomerOrdersAsync(Guid customerId)
        {
            var orders = await _context.Orders.Where(x => x.CustomerId.Equals(customerId)).ToListAsync();
            return new ResultModel<IEnumerable<Order>>
            {
                IsSuccess = true,
                Result = orders
            };
        }

        public async Task<IResultModel<IEnumerable<Order>>> GetAllOrdersAsync()
        {
            var orders = await _context.Orders.ToListAsync();
            return new ResultModel<IEnumerable<Order>>
            {
                IsSuccess = true,
                Result = orders
            };
        }
    }
}