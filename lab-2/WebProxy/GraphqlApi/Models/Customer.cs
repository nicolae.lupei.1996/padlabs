﻿using GR.Core;

namespace GraphqlApi.Models
{
    public class Customer : BaseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}