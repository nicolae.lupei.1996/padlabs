﻿using System;
using GR.Core;

namespace GraphqlApi.Models
{
    public class Order : BaseModel
    {
        public Guid CustomerId { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public decimal Total { get; set; }
    }
}