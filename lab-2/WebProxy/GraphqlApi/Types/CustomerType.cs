﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using GR.Core.Extensions;
using GR.Core.Helpers;
using GraphQL;
using GraphQL.Types;
using GraphqlApi.Models;

namespace GraphqlApi.Types
{
    public class CustomerType : ObjectGraphType<Customer>
    {
        public CustomerType()
        {
            Name = "Customer";
            Field(_ => _.FirstName).Description("Firstname");
            Field(_ => _.LastName).Description("Lastname");

            Field(_ => _.Author);
            Field(_ => _.Changed);
            Field(_ => _.Created);
            Field<StringGraphType>(Name = "Id");
            Field(_ => _.Version);
            Field(_ => _.IsDeleted);
            Field(_ => _.ModifiedBy);
            FieldAsync<ListGraphType<OrderType>>(name: "orders", resolve: async context =>
            {
                var customerId = context.Source.Id;
                try
                {
                    var client = new HttpClient
                    {
                        BaseAddress = new Uri("https://localhost:44314")
                    };
                    var request = await client.GetAsync($"/api/v1/orders/GetCustomerOrders?customerId={customerId}");
                    var data = await request.Content.ReadAsJsonAsync<ResultModel<IEnumerable<Order>>>();
                    return data.Result;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                return context;
            });
        }
    }
}