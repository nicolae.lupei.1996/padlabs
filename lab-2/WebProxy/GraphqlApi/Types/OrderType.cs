﻿using GraphQL.Types;
using GraphqlApi.Models;

namespace GraphqlApi.Types
{
    public class OrderType : ObjectGraphType<Order>
    {
        public OrderType()
        {
            Name = "Orders";
            Field(_ => _.ProductName);
            Field(_ => _.Quantity);
            Field(_ => _.Total);
            Field<StringGraphType>("CustomerId");

            Field(_ => _.Author);
            Field(_ => _.Changed);
            Field(_ => _.Created);
            Field<StringGraphType>(Name = "Id");
            Field(_ => _.Version);
            Field(_ => _.IsDeleted);
            Field(_ => _.ModifiedBy);
        }
    }
}
