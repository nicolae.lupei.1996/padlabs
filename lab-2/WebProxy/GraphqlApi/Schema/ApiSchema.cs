﻿
using GraphQL;
using GraphqlApi.Queries;

namespace GraphqlApi.Schema
{
    public class ApiSchema : GraphQL.Types.Schema
    {
        public ApiSchema(IDependencyResolver resolver) : base(resolver)
        {
            Query = resolver.Resolve<ApiQuery>();
        }
    }
}
