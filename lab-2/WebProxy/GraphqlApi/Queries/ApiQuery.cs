﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using GR.Core.Extensions;
using GR.Core.Helpers;
using GraphQL.Types;
using GraphqlApi.Models;
using GraphqlApi.Types;
using Microsoft.Extensions.Configuration;
using Customer = GraphqlApi.Models.Customer;

namespace GraphqlApi.Queries
{
    public class ApiQuery : ObjectGraphType
    {
        public ApiQuery(IConfiguration configuration)
        {
            var configuration1 = configuration;
            FieldAsync<ListGraphType<CustomerType>>(
                name: "customers", resolve: async context =>
                {
                    var proxyUrl = configuration1.GetValue<string>("ProxyUrl");
                    try
                    {
                        var client = new HttpClient
                        {
                            BaseAddress = new Uri(proxyUrl)
                        };
                        var request = await client.GetAsync("/api/v1/customers/GetAllCustomers");
                        var data = await request.Content.ReadAsJsonAsync<ResultModel<IEnumerable<Customer>>>();
                        return data.Result;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }

                    return context;
                });

            FieldAsync<ListGraphType<OrderType>>(name: "orders", resolve: async context =>
            {
                var proxyUrl = configuration1.GetValue<string>("ProxyUrl");
                try
                {
                    var client = new HttpClient
                    {
                        BaseAddress = new Uri(proxyUrl)
                    };
                    var request = await client.GetAsync("/api/v1/orders/GetAllOrders");
                    var data = await request.Content.ReadAsJsonAsync<ResultModel<IEnumerable<Order>>>();
                    return data.Result;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                return context;
            });
        }
    }
}