using GraphiQl;
using GraphQL;
using GraphQL.Http;
using GraphQL.Types;
using GraphqlApi.Queries;
using GraphqlApi.Schema;
using GraphqlApi.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace GraphqlApi
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGraphiQl(options =>
            {

            });
            services.AddControllers();
            services.AddScoped<IDependencyResolver>(_ => new
                FuncDependencyResolver(_.GetRequiredService));
            services.AddScoped<IDocumentExecuter, DocumentExecuter>();
            services.AddScoped<IDocumentWriter, DocumentWriter>();
            services.AddScoped<ApiQuery>();
            services.AddScoped<CustomerType>();
            services.AddScoped<OrderType>();
            services.AddScoped<ISchema, ApiSchema>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseGraphiQl("/graphql", "/api");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Graphql app is up and running!");
                });

                endpoints.MapControllers();
            });
        }
    }
}
