﻿namespace CustomersApi.ViewModels
{
    public class AddUserDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
