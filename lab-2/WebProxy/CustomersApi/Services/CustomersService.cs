﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using CustomersApi.Abstractions;
using CustomersApi.Data;
using CustomersApi.Models;
using CustomersApi.ViewModels;
using GR.Core.Extensions;
using GR.Core.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace CustomersApi.Services
{
    public class CustomersService : ICustomersService
    {
        #region Injectable

        /// <summary>
        /// Inject context
        /// </summary>
        private readonly CustomersDbContext _context;

        /// <summary>
        /// Mapper
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Inject logger
        /// </summary>
        private readonly ILogger<CustomersService> _logger;

        #endregion

        public CustomersService(CustomersDbContext context, IMapper mapper, ILogger<CustomersService> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<ResultModel<IEnumerable<Customer>>> GetAllCustomersAsync()
        {
            _logger.LogInformation("Get customers called");
            var customers = await _context.Customers.ToListAsync();

            return new ResultModel<IEnumerable<Customer>>
            {
                IsSuccess = true,
                Result = customers
            };
        }

        public async Task<ResultModel<Customer>> GetCustomerByIdAsync(Guid customerId)
        {
            var result = new ResultModel<Customer>();
            var customer = await _context.Customers.AsNoTracking().FirstOrDefaultAsync(x => x.Id.Equals(customerId));
            if (customer == null)
            {
                result.AddError("Customer not found");
                return result;
            }

            result.IsSuccess = true;
            result.Result = customer;
            return result;
        }

        public async Task<ResultModel> UpdateCustomerAsync(UpdateUserDto dto)
        {
            var result = new ResultModel();
            var existentCustomer = await _context.Customers.AsNoTracking().FirstOrDefaultAsync(x => x.Id.Equals(dto.Id));
            if (existentCustomer == null)
            {
                result.AddError("Customer not found");
                return result;
            }

            existentCustomer.FirstName = dto.FirstName;
            existentCustomer.LastName = dto.LastName;
            existentCustomer.Changed = DateTime.Now;
            existentCustomer.Version++;
            _context.Customers.Update(existentCustomer);
            return await _context.SaveAsync();
        }

        public async Task<ResultModel> DeleteCustomerAsync(Guid id)
        {
            var result = new ResultModel();
            var customer = await _context.Customers.AsNoTracking().FirstOrDefaultAsync(x => x.Id.Equals(id));
            if (customer == null)
            {
                result.AddError("Customer not found");
                return result;
            }

            _context.Customers.Remove(customer);

            return await _context.SaveAsync();
        }

        public async Task<ResultModel<Guid>> AddCustomerAsync(AddUserDto dto)
        {
            var customer = _mapper.Map<Customer>(dto);
            await _context.AddAsync(customer);
            var dbResult = await _context.SaveAsync();
            return dbResult.Map(customer.Id);
        }
    }
}