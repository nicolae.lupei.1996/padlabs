using System;
using System.Linq;
using System.Net.Http;
using System.Net.Security;
using System.Text;
using CustomersApi.Abstractions;
using CustomersApi.Data;
using CustomersApi.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AutoMapper;
using Core;
using Core.Dto;
using RpcServices;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CustomersApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private IApplicationBuilder App;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddScoped<ICustomersService, CustomersService>();
            services.AddDbContext<CustomersDbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"));
            });
            services.AddSwaggerGen();
            services.AddAutoMapper(GetType().Assembly);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime applicationLifetime)
        {
            App = app;
            applicationLifetime.ApplicationStarted.Register(OnApplicationStarted);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Customers api is up and running!");
                });
            });

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Customers API");
            });

            UpdateDatabase(app);
        }

        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope();
            using var context = serviceScope.ServiceProvider.GetService<CustomersDbContext>();
            try
            {
                context.Database.Migrate();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private async void OnApplicationStarted()
        {
            var proxyUrl = Configuration.GetValue<string>("ProxyUrl");
            var url = "http://customersapi:80";

            var client = new HttpClient();
            var random = new Random();

            var dto = new RegisterInstanceDto
            {
                InstanceUrl = url,
                MaxRequestsPerMinute = random.Next(100, 1000),
                InstanceType = "customers"
            };

            var postUrl = proxyUrl + "/ServiceDiscovery/Register";
            try
            {
                var registerResult = await client.PostAsync(postUrl, new StringContent(JsonConvert.SerializeObject(dto), Encoding.UTF8, "application/json"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public string GetServerIp()
        {
            var serverAddressesFeature = App.ServerFeatures.Get<IServerAddressesFeature>();
            return serverAddressesFeature.Addresses.First();
        }
    }
}
