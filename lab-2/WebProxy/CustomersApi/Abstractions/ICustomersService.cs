﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CustomersApi.Models;
using CustomersApi.ViewModels;
using GR.Core.Helpers;

namespace CustomersApi.Abstractions
{
    public interface ICustomersService
    {
        Task<ResultModel<IEnumerable<Customer>>> GetAllCustomersAsync();
        Task<ResultModel<Customer>> GetCustomerByIdAsync(Guid customerId);
        Task<ResultModel> UpdateCustomerAsync(UpdateUserDto customer);
        Task<ResultModel> DeleteCustomerAsync(Guid id);
        Task<ResultModel<Guid>> AddCustomerAsync(AddUserDto customer);
    }
}