﻿using GR.Core;

namespace CustomersApi.Models
{
    public class Customer : BaseModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}