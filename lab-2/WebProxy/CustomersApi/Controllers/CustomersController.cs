﻿using System;
using System.Threading.Tasks;
using CustomersApi.Abstractions;
using CustomersApi.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace CustomersApi.Controllers
{
    [ApiController]
    [Route("/api/v1/[controller]/[action]")]
    public class CustomersController : Controller
    {
        #region Injectable

        /// <summary>
        /// Inject customer service
        /// </summary>
        private readonly ICustomersService _customersService;

        #endregion

        public CustomersController(ICustomersService customersService)
        {
            _customersService = customersService;
        }

        [HttpGet]
        public async Task<JsonResult> GetAllCustomers()
        {
            return Json(await _customersService.GetAllCustomersAsync());
        }

        [HttpGet]
        public async Task<JsonResult> GetCustomerById(Guid customerId)
        {
            return Json(await _customersService.GetCustomerByIdAsync(customerId));
        }

        [HttpPost]
        public async Task<JsonResult> AddCustomer(AddUserDto customer)
        {
            return Json(await _customersService.AddCustomerAsync(customer));
        }

        [HttpPost]
        public async Task<JsonResult> UpdateCustomer(UpdateUserDto customer)
        {
            return Json(await _customersService.UpdateCustomerAsync(customer));
        }

        [HttpDelete]
        public async Task<JsonResult> DeleteCustomer(Guid customerId)
        {
            return Json(await _customersService.DeleteCustomerAsync(customerId));
        }
    }
}
