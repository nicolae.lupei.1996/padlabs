﻿using AutoMapper;
using CustomersApi.Models;
using CustomersApi.ViewModels;

namespace CustomersApi.Mappers
{
    public class UsersMapperProfile : Profile
    {
        public UsersMapperProfile()
        {
            CreateMap<Customer, AddUserDto>()
                .IncludeAllDerived()
                .ReverseMap();

            CreateMap<Customer, UpdateUserDto>()
                .IncludeAllDerived()
                .ReverseMap();
        }
    }
}
