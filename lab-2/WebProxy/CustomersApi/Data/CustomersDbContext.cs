﻿using CustomersApi.Models;
using Microsoft.EntityFrameworkCore;

namespace CustomersApi.Data
{
    public class CustomersDbContext : DbContext
    {
        public CustomersDbContext(DbContextOptions<CustomersDbContext> options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
    }
}